import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: PieChart(PieChartData(
            //centerSpaceRadius: double.infinity,
            startDegreeOffset: 0,
            pieTouchData: PieTouchData(
                enabled: true,
                touchCallback: (f) {
                  print("object $f");
                }),
            borderData: FlBorderData(show: false),
            sections: [
              PieChartSectionData(value: 20, color: Colors.green),
              PieChartSectionData(value: 40, color: Colors.yellow)
            ],
          )),
        ),
      ),
    );
  }
}
